CC = gcc
CFLAGS = -O2 -Wall -I ./include

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.
LIB = -lpthread

all: client server

client: ./src/client.c ./src/csapp.o
	$(CC) $(CFLAGS) -o client ./src/client.c ./src/csapp.o $(LIB)

server: ./src/server.c ./src/csapp.o
	$(CC) $(CFLAGS) -o server ./src/server.c ./src/csapp.o $(LIB)

csapp.o: ./src/csapp.c
	$(CC) $(CFLAGS) -c ./src/csapp.c

clean:
	rm -f ./src/*.o client server *~

