#include "csapp.h"

void exeCommand(int connfd, pid_t pid);

int main(int argc, char **argv){

	int listenfd, connfd;
	unsigned int clientlen;
	
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	
	char *haddrp;
	char *port;

	pid_t pid;
	
	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		while(1){
			connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
			if((pid=Fork())==-1){
				Close(connfd);
			}
			if(pid > 0){
				break;
			}
		}
		
		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		exeCommand(connfd, pid);
		Close(connfd);
	}
	exit(0);
}

void trim(char *string){
	string[strlen(string)-1]=0;
}

char* concat(const char *s1, const char *s2){
    char *result = malloc(strlen(s1) + strlen(s2) + 1); 
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

void exeCommand(int connfd, pid_t pid){

	size_t n;
	char buf[MAXLINE];
	rio_t rio;
	pid_t execPID;
	char *args[2];
	
	args[1] = NULL;

	Rio_readinitb(&rio, connfd);
	
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		trim(buf);
		args[0] = concat("/bin/", buf);
		printf("Server Received %lu Bytes\n", n);
		printf("Command: %s\n",buf);
		
		int status;
		if((execPID = fork()) == -1){
			close(connfd);
		}
		if(execPID == 0){
			execve(args[0],args,NULL);
			perror( "Execve Failed" );
    			exit( EXIT_FAILURE);
		}
		else{
			int check = waitpid(-1,&status,0);
			perror("Wait():");
			printf("Check: %d\n",check);
			if(WIFEXITED(status)){
				if (WEXITSTATUS(status) == 0){
					printf("Status: %d\n", status);
					printf("WIFEXITED: %d\n", WIFEXITED(status));
					printf("WEXITSTATUS: %d\n", WEXITSTATUS(status));
				    Rio_writen(connfd, "OK\n", 3);
				}
				else{
				   	Rio_writen(connfd, "ERROR\n", 6);
				}
			}
		}
	}
}
